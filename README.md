# encrusted-android
Android port of encrusted, the Z-machine interpreter.

## Rust part
Can be found at `app/src/main/rust`.

For building the jniLibs use the provided `build.sh` script.
