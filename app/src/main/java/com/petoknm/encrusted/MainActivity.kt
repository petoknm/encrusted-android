package com.petoknm.encrusted

import android.content.Intent
import android.content.Intent.*
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.inputmethod.EditorInfo
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

const val SAVE = 0
const val RESTORE = 1

class MainActivity : AppCompatActivity() {

    private var zvm: ZMachine? = null
    private var buffer = ""
    private var saveData: ByteArray? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        val uri = data?.data ?: return

        if (requestCode == SAVE) {
            val saveData = saveData ?: return
            contentResolver.openOutputStream(uri)?.write(saveData)
            Log.d("SAVE", "ok")
        } else if (requestCode == RESTORE) {
            val zvm = zvm ?: return
            val restoreData = contentResolver.openInputStream(uri)?.readBytes() ?: return
            zvm.restore(restoreData)
            Log.d("RESTORE", "ok")
            zvm.feed("look\n")
            zvm.run()
            buffer += zvm.text()
            updateUI()
        }
    }

    private fun onInput() {
        val zvm = zvm ?: return
        var input = input_text.text.toString()

        if (!input.isEmpty()) {
            input_text.text.clear()
            input += "\n"
            buffer += input

            // divert "restore" event to start a new activity
            if (input == "restore\n") {
                val intent = Intent(ACTION_OPEN_DOCUMENT).apply {
                    type = "*/*"
                    addCategory(CATEGORY_OPENABLE)
                }
                startActivityForResult(intent, RESTORE)
                return
            }

            zvm.feed(input)
        }

        zvm.run()

        saveData = zvm.saveData()

        // save data if present
        if (saveData != null) {
            val intent = Intent(ACTION_CREATE_DOCUMENT).apply {
                type = "*/*"
                addCategory(CATEGORY_OPENABLE)
            }
            startActivityForResult(intent, SAVE)
        }

        buffer += zvm.text()
        updateUI()
    }

    private fun updateUI() {
        val zvm = zvm ?: return
        val (s1, s2) = zvm.statusBar()
        toolbar.title = "$s1 - $s2".trim()
        output_text.text = buffer
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        input_text.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                onInput()
                true
            } else {
                false
            }
        }

        val uri = intent.getParcelableExtra<Uri>("uri")
        val inputStream = contentResolver.openInputStream(uri) ?: throw IllegalArgumentException("Wrong URI")

        zvm = ZMachine(inputStream.readBytes())

        onInput()
    }

}
