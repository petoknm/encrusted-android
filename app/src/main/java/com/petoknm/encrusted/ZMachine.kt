package com.petoknm.encrusted

import java.lang.IllegalArgumentException

class ZMachine(game: ByteArray) {

    private external fun create(game: ByteArray): Long
    private external fun restoreState(l: Long, saveData: ByteArray)
    private external fun run(l: Long)
    private external fun feed(l: Long, s: String)
    private external fun outputText(l: Long): String
    private external fun outputStatusBar1(l: Long): String
    private external fun outputStatusBar2(l: Long): String
    private external fun outputSaveData(l: Long): ByteArray?

    private var ptr: Long = 0

    init {
        ptr = create(game)

        if (ptr == 0L) {
            throw IllegalArgumentException("Zmachine could not be created!")
        }
    }

    fun run() {
        run(ptr)
    }

    fun feed(s: String) {
        feed(ptr, s + "\n")
    }

    fun text(): String {
        return outputText(ptr)
    }

    fun statusBar(): Pair<String, String> {
        val s1 = outputStatusBar1(ptr)
        val s2 = outputStatusBar2(ptr)
        return Pair(s1, s2)
    }

    fun saveData(): ByteArray? {
        return outputSaveData(ptr)
    }

    fun restore(saveData: ByteArray) {
        restoreState(ptr, saveData)
    }

    companion object {
        init {
            System.loadLibrary("encrusted")
        }
    }
}
