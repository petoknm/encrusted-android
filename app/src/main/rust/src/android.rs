use std::str::FromStr;
use std::fmt::Write;

use jni::JNIEnv;
use jni::objects::{JClass, JString, JObject};
use jni::sys::{jlong, jstring, jbyteArray};

use encrusted::{Zmachine, Input, Output};

struct ZvmHandle {
    input: Option<Input>,
    output_text: String,
    output_status_bar: (String, String),
    output_save_data: Option<Vec<u8>>,
    zvm: Zmachine
}

#[no_mangle]
pub extern fn Java_com_petoknm_encrusted_ZMachine_create(env: JNIEnv, _: JClass, game_byte_array: jbyteArray) -> jlong {
    let game = match env.convert_byte_array(game_byte_array) {
        Ok(game) => game,
        _ => return 0
    };

    Box::into_raw(Box::new(ZvmHandle{
        zvm: Zmachine::new(game),
        input: None,
        output_text: "".into(),
        output_status_bar: (String::new(), String::new()),
        output_save_data: None
    })) as jlong
}

#[no_mangle]
pub extern fn Java_com_petoknm_encrusted_ZMachine_restoreState(env: JNIEnv, _: JClass, zvm_handle_ptr: jlong, save_data: jbyteArray) {
    let zvm_handle = unsafe {
        &mut *(zvm_handle_ptr as *mut ZvmHandle)
    };

    match env.convert_byte_array(save_data) {
        Ok(data) => zvm_handle.zvm.handle_restore(data),
        _ => {}
    }
}

#[no_mangle]
pub extern fn Java_com_petoknm_encrusted_ZMachine_run(_: JNIEnv, _: JClass, zvm_handle_ptr: jlong) {
    let zvm_handle = unsafe {
        &mut *(zvm_handle_ptr as *mut ZvmHandle)
    };
    let mut output = Vec::new();
    zvm_handle.zvm.run(zvm_handle.input.take(), &mut output);

    zvm_handle.output_text = output.iter()
        .fold(String::new(), |mut text, output| {
            write!(text, "{}", output).unwrap();
            text
        });

    zvm_handle.output_status_bar = output.iter().rev().cloned()
        .filter_map(|o| match o {
            Output::StatusBar(s1, s2) => Some((s1, s2)),
            _ => None
        })
        .nth(0)
        .unwrap_or_default();

    zvm_handle.output_save_data = output.iter().rev().cloned()
        .filter_map(|o| match o {
            Output::SaveData(data) => Some(data),
            _ => None
        })
        .nth(0);
}

#[no_mangle]
pub extern fn Java_com_petoknm_encrusted_ZMachine_outputText(env: JNIEnv, _: JClass, zvm_handle_ptr: jlong) -> jstring {
    let zvm_handle = unsafe {
        &mut *(zvm_handle_ptr as *mut ZvmHandle)
    };
    env.new_string(&zvm_handle.output_text).unwrap().into_inner()
}

#[no_mangle]
pub extern fn Java_com_petoknm_encrusted_ZMachine_outputStatusBar1(env: JNIEnv, _: JClass, zvm_handle_ptr: jlong) -> jstring {
    let zvm_handle = unsafe {
        &mut *(zvm_handle_ptr as *mut ZvmHandle)
    };
    env.new_string(&zvm_handle.output_status_bar.0).unwrap().into_inner()
}

#[no_mangle]
pub extern fn Java_com_petoknm_encrusted_ZMachine_outputStatusBar2(env: JNIEnv, _: JClass, zvm_handle_ptr: jlong) -> jstring {
    let zvm_handle = unsafe {
        &mut *(zvm_handle_ptr as *mut ZvmHandle)
    };
    env.new_string(&zvm_handle.output_status_bar.1).unwrap().into_inner()
}

#[no_mangle]
pub extern fn Java_com_petoknm_encrusted_ZMachine_outputSaveData(env: JNIEnv, _: JClass, zvm_handle_ptr: jlong) -> jbyteArray {
    let zvm_handle = unsafe {
        &mut *(zvm_handle_ptr as *mut ZvmHandle)
    };

    if let Some(save_data) = zvm_handle.output_save_data.take() {
        env.byte_array_from_slice(&save_data[..]).unwrap()
    } else {
        JObject::null().into_inner()
    }
}

#[no_mangle]
pub extern fn Java_com_petoknm_encrusted_ZMachine_feed(env: JNIEnv, _: JClass, zvm_handle_ptr: jlong, input: JString) {
    let zvm_handle = unsafe {
        &mut *(zvm_handle_ptr as *mut ZvmHandle)
    };
    let input: String = match env.get_string(input) {
        Ok(s) => s.into(),
        _ => "".into(),
    };
    zvm_handle.input = Input::from_str(&input).ok();
}
